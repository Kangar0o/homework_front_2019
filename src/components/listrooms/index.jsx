import React from 'react';
import './styles.css';
import {Link} from 'react-router-dom'

export class ListRooms extends React.Component {
    constructor(props){
        super(props);
        
    }

    render(){
        return <div className='list-room'>
            <Link to={`/room/${this.props.id}`}>
            <img src={this.props.pic} width="400" height="300"/>
            </Link>
            <div className='list-room'>
                {this.props.title} <br/>
                {this.props.adr}
            </div>
        </div>
        ;
    }
}