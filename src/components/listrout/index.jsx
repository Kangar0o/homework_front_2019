import React from 'react';
import './styles.css';
import {Link} from 'react-router-dom'

export class ListRout extends React.Component {
    constructor(props){
        super(props);
        
    }

    render(){
        return <Link className='list-rout' to={`/${this.props.title}`}>
            {this.props.title}  
        </Link>;
    }
}