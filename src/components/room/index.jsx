import React from 'react';
import './styles.css';

export class Room extends React.Component {
    constructor(props){
        super(props);
        
    }

    render(){
        return <div className='fullroom'>
            <div className='room'>
            <img src={this.props.pic} width="400" height="300"/><br/>
            {this.props.title} <br/>
            {this.props.adr}
            </div>
            <div className='date'>
            Расскажите, когда вы хотите снять эту квартиру?<br/>C:
            <input type='date' min='2019-04-04' max='2020-04-04'/><br/>По:
            <input type='date' min='2019-04-04' max='2020-04-04'/><br/>
            <button type='submit'>Проверить возможности брони.</button>
            </div>
        </div>
        ;
    }
}