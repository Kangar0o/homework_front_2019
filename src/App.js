import React, { Component } from 'react';
import './App.css';
import { ListRout } from "./components/listrout";
import { ListRooms } from "./components/listrooms";
import { Room } from "./components/room"

class App extends Component {
  constructor(props) {
		super(props);

		this.state = {
			rooms: [
				{
					id: 1,
					title: 'Room 1',
          adr: 'adr 1',
          pic: 'https://icdn.lenta.ru/images/2018/10/12/12/20181012124943512/pic_d21de13bd4af609ddf5984202d7f75eb.jpg'
				},
				{
					id: 2,
					title: 'Room 2',
          adr: 'adr 2',
          pic: 'http://i-avstraliya.ru/wp-content/uploads/2013/09/kvartira-v-avstralii.jpg'
				},
				{
          id: 3,
					title: 'Room 3',
          adr: 'adr 3',
          pic: 'http://pogostim.com/content/uploader/image/info/new/Rasrusheniye_mifov_o_posutochnoy_sdache_kvartir_kvartira_posutochno.jpg'
				},
				{
          id: 4,
					title: 'Room 4',
          adr: 'adr 4',
          pic: 'https://standart11.ru/catalog/view/theme/standart/image/apartment/kommun_85.jpg'
				},
				{
          id: 5,
					title: 'Room 5',
          adr: 'adr 5',
          pic: 'http://www.anfilada-design.ru/test/wp-content/gallery/novopeskovskij-per-195kv-m/dynamic/178.jpg-nggid03243-ngg0dyn-350x270x100-00f0w010c011r110f110r010t010.jpg'
        },
        {
          id: 6,
					title: 'Room 6',
          adr: 'adr 6',
          pic: 'https://i.ytimg.com/vi/umn3ELodKYI/hqdefault.jpg'
				},
      ],
      rout: [
        {
          title: 'Бронь'
        },
        {
          title: 'Отзывы'
        }, 
        {
          title: 'Контакты'
        }
      ]
		};
	}

  render() {
    
    let mainpage = <div className="App-rooms">
      {this.state.rooms.map((item, index) =>
      <ListRooms title={item.title} id={item.id}
       adr={item.adr} pic={item.pic} />)} 
    </div>;
    if(this.props.match.params.routid == 'Контакты'){ 
      mainpage = <div className='Label'>
      Напиши мне:<br/>
      @Kangaroo<br/>
      +79262801207<br/>
      vk.com/warnir
      </div>;
    }
    if(this.props.match.params.routid == 'Отзывы'){
      mainpage = <div className='Label'>
      Пасхальный кролик!<br/>
      Дай знать, если ты действительно веришь в то,<br/>
      что я выложу здесь не бесполезные хвалебные отзывы!
      </div>;
    }
    if(this.props.match.params.roomid != undefined){
      let buff = this.state.rooms[0];
      switch(this.props.match.params.roomid){
        case '1':break;
        case '2':
        buff = this.state.rooms[1];
        break;
        case '3':
        buff = this.state.rooms[2];
        break;
        case '4':
        buff = this.state.rooms[3];
        break;
        case '5':
        buff = this.state.rooms[4];
        break;
        case '6':
        buff = this.state.rooms[5];
        default:
        
        break;
      }

      if(buff){
          mainpage = <div className='App-rooms'>
            <Room title={buff.title} 
            id={buff.id} 
            adr={buff.adr}
            pic={buff.pic}/></div>;
      }
    }
    return (
        <div className="App">
        <div className="App-logo">
        Minsk-Rooms <br/>
        Минские квартиры для удобного туризма.
        </div>
        <div className="App-rout">
          {this.state.rout.map((item, index) =>
            <ListRout title={item.title} />)} 
        </div>
        {mainpage}
      </div>
    );
  }
}

export default App;
